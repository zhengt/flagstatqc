# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.utils import timezone

class Runfolder(models.Model):
    runfolder_name = models.CharField(unique=True, max_length=50)
    total_reads = models.IntegerField(blank=True, null=True)
    #runfolder_total_failed = models.IntegerField(default=0)
    mapped_reads = models.IntegerField(blank=True, null=True)
    #percentage_of_mapped_reads = models.FloatField(default=0)
    #runfolder_mapped_failed = models.IntegerField(default=0)
    duplicate_reads = models.IntegerField(blank=True, null=True)
    import_date = models.DateTimeField()
    #sample_count = models.IntegerField(blank=True, null=True)
    #percentage_of_duplicate_reads = models.FloatField(default=0)
    #runfolder_duplicates_failed = models.IntegerField(default=0)

    class Meta:
        # managed = False will prevent any changes to the structure and content of the Runfolder table in the NGS QC flagstat database.
        managed = False
        db_table = 'runfolder'

    
    def calculating_runfolder_stat(self):

        print ("Calculating stats for runfolder: {}".format(self.runfolder_name))

        total_reads = 0
        mapped_reads = 0
        duplicate_reads = 0
        sample_count = 0
        
        for sample in Sample.objects.filter(runfolder_id__exact = self.id):
            count = 0
            if not str(sample.in_total_passed).isdigit():
                count += 1
            if not str(sample.mapped_passed).isdigit():
                count += 1
            if not str(sample.duplicates_passed).isdigit():
                count += 1
            if count:
                print ("{}/{} not numbers".format(sample.sample_name, count))
                continue

            total_reads += sample.in_total_passed
            mapped_reads += sample.mapped_passed
            duplicate_reads += sample.duplicates_passed
            sample_count += 1

        if sample_count == 0:
            print ("no sample info available")
            return self

        self.sample_count = sample_count
        self.total_reads = total_reads
        self.mapped_reads = mapped_reads
        self.duplicate_reads = duplicate_reads
        self.save()
        return self

    def __str__(self):
       return self.runfolder_name


class Sample(models.Model):
    sample_name = models.CharField(unique=True, max_length=20)
    runfolder = models.ForeignKey(Runfolder, on_delete=models.CASCADE)
    in_total_passed = models.IntegerField(blank=True, null=True)
    #in_total_failed = models.IntegerField(default=0)
    mapped_passed = models.IntegerField(blank=True, null=True)
    #mapped_failed = models.IntegerField(default=0)
    duplicates_passed = models.IntegerField(blank=True, null=True)
    #duplicates_failed = models.IntegerField(default=0)
    import_date = models.DateTimeField()

    def __str__(self):
        return self.sample_name 

    class Meta:
        # managed = False will prevent any changes to the structure and content of the Sample table in the NGS QC flagstat database.
        managed = False
        db_table = 'sample'

