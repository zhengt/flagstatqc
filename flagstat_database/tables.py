import django_tables2 as tables
from django_tables2.utils import A


class Runfolder_stats_table(tables.Table):

	runfolder_name = tables.LinkColumn('runfolder', args=[A('id')], verbose_name='Runfolder_name')
	total_reads =  tables.Column(verbose_name="Total reads")
	pcent_mapped = tables.Column(verbose_name="% of mapped reads")
	pcent_duplicates = tables.Column(verbose_name="% of duplicate reads")
	sample_count = tables.Column(verbose_name="sample numbers")
	import_date = tables.Column(verbose_name="Date and time of last data import")

	class Meta:
		attrs = {"class": "paleblue"}
	
	def render_total_reads(self, value):
		return large_number(value)


class Sampletable(tables.Table):

	sample_name = tables.LinkColumn('sample', args=[A('id')], verbose_name='Sample_name')
	total_reads = tables.Column(verbose_name="Total reads")
	pcent_mapped = tables.Column(verbose_name="% of mapped reads")
	pcent_duplicates = tables.Column(verbose_name="% of duplicate reads")
	import_date = tables.Column(verbose_name="date and time of last data import")

	class Meta:
		attrs = {"class": "paleblue"}

	def render_total_reads(self, value):
		return large_number(value)

	def render_pcent_mapped(self, value):
		return "{:.2f}".format(value)

	def render_pcent_duplicates(self, value):
		return "{:.2f}".format(value)

def large_number(value):
	if type(value) is not int and type(value) is not float:
		return value

	if value >= 1000000000:
		value = float(value) / 1000000000
		value ="{:.2f}{}".format(value, "B")

	elif value >= 1000000:
		value = float(value) / 1000000
		value ="{:.2f}{}".format(value, "M")

	elif value >= 1000:
		value = float(value) / 1000
		value = "{:.2f}{}".format(value, "K")

	elif type(value) is float:
		value = "{:.2f}".format(value)
	return value