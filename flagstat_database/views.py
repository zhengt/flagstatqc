from django.shortcuts import render, get_object_or_404
#from django.http import HttpResponse, Http404
from .models import Sample, Runfolder
from django.forms.models import model_to_dict
from django_tables2 import RequestConfig
import flagstat_database.models as Models
import flagstat_database.tables as Tables
from operator import itemgetter
import pprint as pp

def is_number(value):
	# checks if the value is a number or not, if it is it is returned, otherwise returns False
	if type(value) is int or type(value) is float:
		return True
	return False

def large_number(value):
	# converts large numbers into readable ones in units of 1000=K, 1000000=M, 1000000000=B
	if type(value) is not int and type(value) is not float:
		return value

	if value >= 1000000000:
		value = float(value) / 1000000000
		value ="{:.2f}{}".format(value, "B")

	elif value >= 1000000:
		value = float(value) / 1000000
		value ="{:.2f}{}".format(value, "M")

	elif value >= 1000:
		value = float(value) / 1000
		value = "{:.2f}{}".format(value, "K")

	elif type(value) is float:
		value = "{:.2f}".format(value)
	return value

def index(request):
    latest_runfolders_stat = Runfolder.objects.order_by('runfolder_name')[::-1][:5]
    return render(request, 'flagstat_database/index.html', {'latest_runfolders_stat': latest_runfolders_stat})

def about(request):
	flaginfo = "https://broadinstitute.github.io/picard/explain-flags.html"
	context = {'flaginfo': flaginfo}
	return render(request, 'flagstat_database/about.html', context)

def runfolders(request):

	runfoldercount = len(list(Models.Runfolder.objects.all()))
	runfolders = list(Models.Runfolder.objects.all())
	samples = list(Models.Sample.objects.all())
	runfolder_averages = []
	runfolder_id = []
	sample_count_per_runfolder = []

	for runfolder in runfolders:
		if not is_number(runfolder.total_reads):	
			runfolder = Models.Runfolder.calculating_runfolder_stat(runfolder)
		runfolder = model_to_dict(runfolder)

		if runfolder['id'] is not None and runfolder['id'] not in runfolder_id:
			runfolder_id.append(runfolder['id'])

	for ids in runfolder_id:
		count = 0
		for sample in samples:
			if sample.runfolder_id == ids:
				count += 1
		sample_count_per_runfolder.append(count)

	sample_number_dict = dict(zip(runfolder_id, sample_count_per_runfolder))
	#pp.pprint(sample_number_dict)

	for runfolder in runfolders:

		#if runfolder does not have calculated stats for total_reads, then calculate them
		if not is_number(runfolder.total_reads):	
			runfolder = Models.Runfolder.calculating_runfolder_stat(runfolder)

		runfolder = model_to_dict(runfolder)

		if runfolder['total_reads'] is not None:
			runfolder['total_reads'] = runfolder["total_reads"]
		#else:
			#runfolder['total_reads'] = 0

		if is_number(runfolder['mapped_reads']) and runfolder['mapped_reads'] != 0 and is_number(runfolder['total_reads']) and runfolder['total_reads'] != 0:
			runfolder['pcent_mapped'] = "{:.2f}".format(runfolder['mapped_reads']*100.0/runfolder['total_reads'])
		#else:
			#runfolder['pcent_mapped'] = 0 
			#"either mapped_reads/total_reads not valid number"

		if is_number(runfolder['duplicate_reads']) and runfolder['duplicate_reads'] != 0 and is_number(runfolder['mapped_reads']) and runfolder['mapped_reads'] != 0:
			runfolder['pcent_duplicates'] = "{:.2f}".format(runfolder['duplicate_reads']*100.0/runfolder['mapped_reads'])
		#else:
			#runfolder['pcent_duplicates'] = 0 #"either duplicate_reads/mapped_reads not valid number"
		if runfolder['id'] is not None and runfolder['id'] in sample_number_dict.keys():
			runfolder['sample_count'] = sample_number_dict[runfolder['id']]
		#pp.pprint (runfolder)

		runfolder_averages.append(runfolder)

	stats_table = Tables.Runfolder_stats_table(runfolder_averages)
	# sort runfolders from last to first
	stats_table.order_by = "-runfolder_name"
	context = {'stats_table': stats_table}
	context["runfoldercount"] = runfoldercount
	RequestConfig(request).configure(stats_table)

	#render(request, 'flagstat_database/runfolders.html', stat)
	return render(request, 'flagstat_database/runfolders.html', context)

def runfolder(request, pk):
	runfolder_stats = list(Models.Sample.objects.filter(runfolder_id=pk))

	stats = []
	average = {}
	samples = 0

	for runfolder_stat in runfolder_stats:
		
		if is_number(runfolder_stat.in_total_passed) and runfolder_stat.in_total_passed != 0 and is_number(runfolder_stat.mapped_passed) and runfolder_stat.mapped_passed != 0:
			runfolder_stat.pcent_mapped = runfolder_stat.mapped_passed*100.0/runfolder_stat.in_total_passed
		
		if is_number(runfolder_stat.duplicates_passed) and runfolder_stat.duplicates_passed != 0 and is_number(runfolder_stat.mapped_passed) and runfolder_stat.mapped_passed != 0:
			runfolder_stat.pcent_duplicates = runfolder_stat.duplicates_passed*100.0/runfolder_stat.mapped_passed
		
		samples += 1
		
		runfolder_stat = model_to_dict(runfolder_stat)

		if runfolder_stat['in_total_passed'] is None or runfolder_stat['in_total_passed'] == 0:
			runfolder_stat['total_reads'] = -1
		else:
			runfolder_stat['total_reads'] = runfolder_stat['in_total_passed']

		if runfolder_stat['mapped_passed'] is None or runfolder_stat['mapped_passed'] == 0:
			runfolder_stat['mapped_reads'] = -1

		if runfolder_stat['duplicates_passed'] is None or runfolder_stat['duplicates_passed'] == 0:
			runfolder_stat['duplicate_reads'] = -1

		if runfolder_stat['mapped_passed'] == 0 or runfolder_stat['total_reads'] == 0:
			continue #print ("mapped_passed/in_total_passed for this runfolder = 0")
		else:
			runfolder_stat['pcent_mapped'] = runfolder_stat['mapped_passed']*100.0/runfolder_stat['total_reads']
			
		if runfolder_stat['duplicates_passed'] == 0 or runfolder_stat['mapped_passed'] == 0:
			continue #print ("duplicates_passed/in_total_passed for this runfolder = 0")
		else:
			runfolder_stat['pcent_duplicates'] = runfolder_stat['duplicates_passed']*100.0/runfolder_stat['mapped_passed']

		for item in runfolder_stat:

			if type(runfolder_stat[item]) is not float and type(runfolder_stat[item]) is not int:
				continue

			if item not in average:
				average[item] = 0

			average[item] += runfolder_stat[item]

		stats.append(runfolder_stat)

	for item in average:
		if type(runfolder_stat[item]) is float:
			average[item] = average[item]*1.0/samples

		average[item] = large_number(average[item])

	runfolder_table = Tables.Sampletable(stats)

	context = {'runfolder_table': runfolder_table}
	context['runfolder_stats'] = stats
	runfolder = Models.Runfolder.objects.get(pk=pk)
	#runfolder = get_object_or_404(Runfolder, pk=pk)
	context['runfolder_name'] = runfolder.runfolder_name
	context['average'] = average
	RequestConfig(request).configure(runfolder_table)
	
	return render(request, 'flagstat_database/runfolder.html', context)
	
def samples(request):
	samplecount = len(list(Models.Sample.objects.all()))
	#stat = {'samplecount': samplecount}
	samples = Sample.objects.order_by('sample_name')[::-1]
	context = {"samples": samples}
	context["samplecount"] = samplecount
	return render(request, 'flagstat_database/samples.html', context)

def sample(request, pk):
	sample = get_object_or_404(Sample, pk=pk)
	runfolder = Models.Runfolder.objects.get(pk=sample.runfolder_id)
	#in_total_failed = sample.objects.filter(Sample__in_total_failed=in_total_failed)
	context = {'sample': sample}
	context["runfolder"] = runfolder
	#context = {'runfolder': runfolder}
	return render(request, 'flagstat_database/sample.html', context)

""" Currently the search function is still under development"""
"""def search(max_results=0, starts_with=''):

	search_list = []
	
	if starts_with:

		search_list = [{'value':x.sample_name} for x in Models.Sample.objects.filter(name__isstartswith=starts_with)[:max_results]][:5]
		search_list+= [{'value':x.runfolder_name} for x in Models.Runfolder.objets.filter(name__isstartswith=starts_with)[:max_results]][:5]

		search_list = sorted(search_list)

	if max_results > 0:
		if search_list and len(search_list) > max_results:
			search_list = search_list[:max_results]

	return search_list"""


