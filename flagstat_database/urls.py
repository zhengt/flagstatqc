from django.conf.urls import url, include
from django.contrib import admin
#from django.contrib.staticfiles.urls import staticfiles_urlpatterns
#from django.views.generic import TemplateView
from . import views

#app_name= 'flagstat_database'
urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^about/$', views.about, name='about'),
	url(r'^runfolders/$', views.runfolders, name='runfolders'),
	url(r'^runfolder/(?P<pk>\d+)/$', views.runfolder, name='runfolder'),
	url(r'^samples/$', views.samples, name='samples'),
	url(r'^sample/(?P<pk>\d+)/$', views.sample, name='sample'),
	#url(r'^parameters/', views.parameters, name='parameters'),
	#url(r'^samplerunfolder/<int:srfid>$', views.srfid, name='srfid'),
	] 