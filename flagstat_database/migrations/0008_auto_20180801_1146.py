# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-08-01 10:46
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('flagstat_database', '0007_runfolder_sample_count'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='runfolder',
            options={'managed': False},
        ),
    ]
