from django.apps import AppConfig


class FlagstatDatabaseConfig(AppConfig):
    name = 'flagstat_database'
