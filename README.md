# NGS QC Web App
By Tengyue Zheng
27/06/2017

How to view the webpages:
1. git clone the repository to working directory in your user area.
2. Create a virtual environment and pip install python3, django 1.11 and django-tables2
3. Activate the virtual environment
4. run python manage.py runserver login01.ctrulab.uk:8010

## Main scripts:
- /djangoproject2/settings.py
- /djangoproject2/manage.py
- /djangoproject2/flagstat_database/models.py
- /djangoproject2/urls.py
- /djangoproject2/flagstat_database/views.py
- /djangoproject2/flagstat_database/urls.py
- /djangoproject2/flagstat_database/templates/flagstat_database/base.html

## Languages and platform used:
NGS QC web app using Python 3.4.5
Django 1.11

To clone the Git repository go here: https://github.com/tz2614/flagstatapp

## User Requirements:
Enable users to search within NGS QC database and retrieve QC information about each sample within any NGS runfolder, 
and observe any trends overtime.

## User Stories:
- Login into the cluster and deploy the NGS QC web app
- Navigate to the login.ctrulab.uk:8010/NGS_QC web page from a web browser
- Click on **Samples** or **Runfolders** to navigate to the samples or runfolders page
- Scroll down, click prev or next button, or Ctrl+F and type the sample name or runfolder name to find the runfolder or sample of your interest
- View the data adjacent to the names of sample or runfolder.
- Click on the sample name and runfolder name to display the data for that sample or runfolder.

## Instructions to set up the web app from scratch:
1. Using Linux OS in the MobaXTerm environment, create a working directory folder within your user area called djangoproject2
/mnt/storage/home/zhengt/djangoproject/djangoproject2

2. Following a the Django 1.11 tutorial from https://docs.djangoproject.com/en/1.11, create a new django app called flagstatapp

3. Use a text editor and edit the settings.py to include 
```Python
ALLOWED_HOSTS = ["login01.ctrulab.uk"]
INSTALLED_APPS = ['flagstat_database.apps.FlagstatDatabaseConfig',
    'django_tables2','django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',    
]
ROOT_URLCONF = 'djangoproject2.urls'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': '/mnt/storage/home/zhengt/djangoproject/djangoproject2/flagstat/flagstat_db/flagstat_2.1.db',
    }
}
```
4. Create a separate directory within djangoproject2/ called flagstat_database/ to house Django scripts including models.py, urls.py, views.py and templates/
5. In the djangoproject2/ directory, create models.py and create classes for samples and runfolders tables, 
which include QC information for each NGS sample, for example, parameters from each sample flagstat e.g. total number of reads passed.
#complete
6. In the djangoproject2/ directory, create urls.py that routes URLs to views.py script, 
including the urls for admin and flagstat_database.urls (which will later become NGS QC urls).
#complete
7. In the djangoproject2/ directory, create views.py that delivers the index, samples, runfolders, sample and runfolder pages.
#complete
8. In the djangoproject2/ directory to store all templates called "templates", and within the templates/, create a another directory called "flagstat_database". In templates/flagstat_database/, create a base.html template for other web pages to expand from.
#complete
9. In the templates/flagstat_database/ directory, create index.html, samples.html (containing QC data for all samples, and date created), runfolders.html(containing web links to each runfolder, and date created), sample.html(QC information for each sample, sample name and date created) and runfolder (QC information for each runfolder and date created).html

## Database creation:
Please see https://github.com/tz2614/flagstat_db repository for information on how to extract QC information from runfolders into the database, and unit testing performed.
