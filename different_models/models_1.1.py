# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Parameters(models.Model):
    parameter_id = models.IntegerField(blank=True, null=True)
    in_total_passed = models.IntegerField()
    in_total_failed = models.IntegerField()
    duplicates_passed = models.IntegerField()
    duplicates_failed = models.IntegerField()
    mapped_passed = models.IntegerField()
    mapped_failed = models.IntegerField()
    paired_in_sequencing_passed = models.IntegerField()
    paired_in_sequencing_failed = models.IntegerField()
    read1_passed = models.IntegerField()
    read1_failed = models.IntegerField()
    read2_passed = models.IntegerField()
    read2_failed = models.IntegerField()
    properly_paired_passed = models.IntegerField()
    properly_paired_failed = models.IntegerField()
    with_itself_and_mate_mapped_passed = models.IntegerField()
    with_itself_and_mate_mapped_failed = models.IntegerField()
    singletons_passed = models.IntegerField()
    singletons_failed = models.IntegerField()
    with_mate_mapped_to_a_different_chr_passed = models.IntegerField()
    with_mate_mapped_to_a_different_chr_failed = models.IntegerField()
    with_mate_mapped_to_a_different_chr_mapq5_passed = models.IntegerField()
    with_mate_mapped_to_a_different_chr_mapq5_failed = models.IntegerField()
    srfid = models.ForeignKey('SampleRunfolder', models.DO_NOTHING, db_column='srfid')

    class Meta:
        managed = False
        db_table = 'parameters'


class Runfolder(models.Model):
    rfid = models.IntegerField(blank=True, null=True)
    rf_name = models.TextField()

    class Meta:
        managed = False
        db_table = 'runfolder'


class Sample(models.Model):
    sid = models.IntegerField(blank=True, null=True)
    sample_name = models.TextField()

    class Meta:
        managed = False
        db_table = 'sample'


class SampleRunfolder(models.Model):
    srfid = models.IntegerField(blank=True, null=True)
    sid = models.IntegerField()
    rfid = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'sample_runfolder'
