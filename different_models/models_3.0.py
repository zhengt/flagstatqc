# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.utils import timezone

class Sample(models.Model):
    s_id = models.IntegerField(primary_key=True, null=False)
    sample_name = models.CharField(max_length=20)
    rf_id = models.ForeignKey(Runfolder, on_delete=models.CASCADE,db_column='rf_id')
    in_total_passed = models.IntegerField()
    in_total_failed = models.IntegerField()
    mapped_passed = models.IntegerField()
    duplicates_passed = models.IntegerField()
    duplicates_failed = models.IntegerField()
    #created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.sample_name

    class Meta:
        #managed = False
        db_table = 'sample'

class Runfolder(models.Model):
    rf_id = models.IntegerField(primary_key=True, null=False)
    rf_name = models.CharField(max_length=50)
    s_id = models.ForeignKey(Sample, on_delete=models.CASCADE,db_column='s_id')
    #created_date = models.DateTimeField(default=timezone.now)
    
    def __str__(self):
        return self.rf_name

    class Meta:
        #managed = False
        db_table = 'runfolder'

