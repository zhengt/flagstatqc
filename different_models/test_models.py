from django.db import models
"""
class Sample(models.Model):

	sid = models.primary_key(unique=True)
	sample_name = models.CharField(max_length=20)
    #test_date = models.DateField()

    def __str__(self)
    Return "the sample is {} and was tested on {}".format(self.sample_name, self.test_date)


class Runfolder(models.Model)
    
    rfid = models.primary_key(unique=True)
    rf_name = models.CharField(max_length=50)
    run_date = models.DateField()

    def __str__(self)
    Return "the runfolder is {} and was ran on {}".format(self.rf_name, self.run_date)

class Sample_runfolder(models.Model)

    srfid = models.primary_key(unique=True)
    rfid = models.ForeignKey(Runfolder, on_delete=models.CASCADE)
    sid = models.ForeignKey(Sample, on_delete=models.CASCADE)

    
class Parameters(models.Model)

    parameter_id = models.primary_key(unique=True)
    in_total_passed = models.PositiveIntegerField() 
    duplicates_passed = models.PositiveIntegerField() 
    duplicates_failed = models.PositiveIntegerField() 
    mapped_passed = models.PositiveIntegerField() 
    mapped_failed = models.PositiveIntegerField() 
    paired_in_sequencing_passed = models.PositiveIntegerField() 
    paired_in_sequencing_failed = models.PositiveIntegerField() 
    read1_passed = models.PositiveIntegerField() 
    read1_failed = models.PositiveIntegerField()  
    read2_passed = models.PositiveIntegerField() 
    read2_failed = models.PositiveIntegerField()  
    properly_paired_passed = models.PositiveIntegerField() 
    properly_paired_failed = models.PositiveIntegerField() 
    with_itself_and_mate_mapped_passed = models.PositiveIntegerField() 
    with_itself_and_mate_mapped_failed = models.PositiveIntegerField()  
    singletons_passed = models.PositiveIntegerField() 
    singletons_failed = models.PositiveIntegerField() 
    with_mate_mapped_to_a_different_chr_passed = models.PositiveIntegerField() 
    with_mate_mapped_to_a_different_chr_failed = models.PositiveIntegerField() 
    with_mate_mapped_to_a_different_chr_MapQ5_passed = models.PositiveIntegerField() 
    with_mate_mapped_to_a_different_chr_MapQ5_failed = models.PositiveIntegerField()  
    srfid = models.ForeignKey(Sample_runfolder, on_delete=models.CASCADE) 

    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    #def publish(self):
        #self.published_date = timezone.now()
        #self.save()

"""